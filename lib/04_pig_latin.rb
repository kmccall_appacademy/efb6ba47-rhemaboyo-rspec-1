def translate(str)
  words = str.split(' ')
  i = 0
  words.length.times do
    words[i] = pigin(words[i])
    i += 1
  end
  words.join(' ')
end

def pigin(str)
  word = str.chars
  beginning = ''
  ending = ''
  i = 0
  while beginning.length < word.length
    letter = word[i]
    if letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u'
      beginning += word.join('')
    elsif letter == 'q' && word[i + 1] == 'u'
      2.times { ending += word.shift }
    else
      ending += word.shift
    end
  end
  beginning + ending + 'ay'
end
