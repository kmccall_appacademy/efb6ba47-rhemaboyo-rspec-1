def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  sum = 0
  array.each do |num|
    sum += num
  end
  sum
end

def multiply(*nums)
  product = 1
  nums.each do |num|
    product *= num
  end
  product
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  if num <= 1
    1
  else
    num * factorial(num - 1)
  end
end
