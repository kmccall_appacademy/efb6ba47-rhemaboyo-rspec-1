require 'byebug'

def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, num = 2)
  ([str] * num).join(' ')
end

def start_of_word(str, num)
  letters = str.chars
  result = ''
  num.times do
    result += letters.shift
  end
  result
end

def first_word(str)
  words = str.split
  words[0]
end

def titleize(str)
  words = str.split
  i = 0
  words.length.times do
    if (words[i].length > 3 && words[i] != 'over') || i == 0 
      words[i] = words[i].capitalize
    end
    i += 1
  end
  words.join(' ')
end
